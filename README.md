# CAD for smart home

## Usage:
* Create a new schema - creates a new schema with your image
* Right click for create new sensor


## Changelist
* 0.4.2
  * Crated optimizing function for positions
* 0.4.1
  * Created script for MQTT server
    to check sensors' values
* 0.4.0
  * Sensors' radius created
* 0.3.4
  * Old sensors deleted
  * Movable sensors
* 0.3.3
  * Edit mainWindow scaling
  * Disable scrollbars
* 0.3.2
  * Change red dots to chip images
* 0.3.1
  * Added code generator for publishers
* 0.3.0
  * Add .ino templates files
  * Compilation works!
  * Compiled file example [device0.ino](device0.ino)
  * Added links in readme
* 0.2.5
  * Delete old points when loads hcad
  * Minor bug fix
* 0.2.4
  * Load from file works
  * Add [example.hcad](example.hcad) file
* 0.2.3
  * Scale image if bigger then screen
* 0.2.2
  * Fixed screen size
* 0.2.1
  * Saving algorithm works. The saving format - YAML. Saving extension - hcad
* 0.2.0
  * Rewritten code for use with graphicsScene
  * Created class for storage and use devices
  * Created compile menu entry
  * Created dialog menu for save button
  * Merge from graphics_layout into master 
* 0.1.2
  * Change structure of device list
* 0.1.1
  * Delete CHANGELOG.md
* 0.1.0
    * Created logic for create sensors
    * Stable version
* 0.0.8
    * Create sensor ui design done
    * Add test file
* 0.0.7
    * New UI for Create Sensor
* 0.0.6:
    * created class for UI
    * right click item listener
* 0.0.5:
    * created right-click context menu
* 0.0.4:
    * created schema picker with your image
* 0.0.3:
    * fix gui path
* 0.0.2:
    * created initial design
* 0.0.1:
    * Project created