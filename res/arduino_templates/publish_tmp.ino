  int %s_state = digitalRead(%s);
  Serial.print(F("\nSending state val "));
  Serial.print(%s_state, BIN);
  Serial.print("...");
  if (! %s.publish(%s_state)) {
    Serial.println(F("Failed"));
  } else {
    Serial.println(F("OK!"));
  }

