#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

#define WLAN_SSID "ssid"
#define WLAN_PASS "passwd"

#define AIO_SERVER "tech.esp-one.com"
#define AIO_SERVERPORT 1883
#define AIO_USERNAME "login"
#define AIO_KEY "passwd"

#define TEMP D8
#define LIGHT A0
#define MOTION D7
#define WATER D6
#define LIGHT_SWITCH D5
#define DEVICE_SWITCH D4

WiFiClient client;

Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);
Adafruit_MQTT_Subscribe tempSensor = Adafruit_MQTT_Subscribe(&mqtt, "home/device0/TEMP");
Adafruit_MQTT_Subscribe lightSensor = Adafruit_MQTT_Subscribe(&mqtt, "home/device0/LIGHT");
Adafruit_MQTT_Subscribe motionSensor = Adafruit_MQTT_Subscribe(&mqtt, "home/device0/MOTION");
Adafruit_MQTT_Subscribe waterSensor = Adafruit_MQTT_Subscribe(&mqtt, "home/device0/WATER");
Adafruit_MQTT_Subscribe light_switchSensor = Adafruit_MQTT_Subscribe(&mqtt, "home/device0/LIGHT_SWITCH");
Adafruit_MQTT_Subscribe deviceSensor = Adafruit_MQTT_Subscribe(&mqtt, "home/device0/DEVICE_SWITCH");

void MQTT_connect();

void setup() {
  Serial.begin(115200);
  delay(10);

  pinMode(TEMP, OUTPUT);
  digitalWrite(TEMP, LOW);
  pinMode(LIGHT, OUTPUT);
  digitalWrite(LIGHT, LOW);
  pinMode(MOTION, OUTPUT);
  digitalWrite(MOTION, LOW);
  pinMode(WATER, OUTPUT);
  digitalWrite(WATER, LOW);
  pinMode(LIGHT_SWITCH, OUTPUT);
  digitalWrite(LIGHT_SWITCH, LOW);
  pinMode(DEVICE_SWITCH, OUTPUT);
  digitalWrite(DEVICE_SWITCH, LOW);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);   //Lights when not connected to MQTT

  // Connect to WiFi access point.
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  mqtt.subscribe(&tempSensor);
  mqtt.subscribe(&lightSensor);
  mqtt.subscribe(&motionSensor);
  mqtt.subscribe(&waterSensor);
  mqtt.subscribe(&light_switchSensor);
  mqtt.subscribe(&deviceSensor);

}

void loop() {

  MQTT_connect();

  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(3000))) {
    if (subscription == &tempSensor) {
      Serial.print(F("Got: "));
      Serial.println((char *)tempSensor.lastread);
      if (strcmp((char *)tempSensor.lastread, "1") == 0) {
        digitalWrite(TEMP, HIGH);
      }
      if (strcmp((char *)tempSensor.lastread, "0") == 0) {
        digitalWrite(TEMP, LOW);
      }
    }
    if (subscription == &lightSensor) {
      Serial.print(F("Got: "));
      Serial.println((char *)lightSensor.lastread);
      if (strcmp((char *)lightSensor.lastread, "1") == 0) {
        digitalWrite(LIGHT, HIGH);
      }
      if (strcmp((char *)lightSensor.lastread, "0") == 0) {
        digitalWrite(LIGHT, LOW);
      }
    }
    if (subscription == &motionSensor) {
      Serial.print(F("Got: "));
      Serial.println((char *)motionSensor.lastread);
      if (strcmp((char *)motionSensor.lastread, "1") == 0) {
        digitalWrite(MOTION, HIGH);
      }
      if (strcmp((char *)motionSensor.lastread, "0") == 0) {
        digitalWrite(MOTION, LOW);
      }
    }
    if (subscription == &waterSensor) {
      Serial.print(F("Got: "));
      Serial.println((char *)waterSensor.lastread);
      if (strcmp((char *)waterSensor.lastread, "1") == 0) {
        digitalWrite(WATER, HIGH);
      }
      if (strcmp((char *)waterSensor.lastread, "0") == 0) {
        digitalWrite(WATER, LOW);
      }
    }
    if (subscription == &light_switchSensor) {
      Serial.print(F("Got: "));
      Serial.println((char *)light_switchSensor.lastread);
      if (strcmp((char *)light_switchSensor.lastread, "1") == 0) {
        digitalWrite(LIGHT_SWITCH, HIGH);
      }
      if (strcmp((char *)light_switchSensor.lastread, "0") == 0) {
        digitalWrite(LIGHT_SWITCH, LOW);
      }
    }
    if (subscription == &deviceSensor) {
      Serial.print(F("Got: "));
      Serial.println((char *)deviceSensor.lastread);
      if (strcmp((char *)deviceSensor.lastread, "1") == 0) {
        digitalWrite(DEVICE_SWITCH, HIGH);
      }
      if (strcmp((char *)deviceSensor.lastread, "0") == 0) {
        digitalWrite(DEVICE_SWITCH, LOW);
      }
    }

  }

}

void MQTT_connect() {
  int8_t ret;

  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
    Serial.println(mqtt.connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    mqtt.disconnect();
    delay(5000);  // wait 5 seconds
    retries--;
    if (retries == 0) {
      // basically die and wait for WDT to reset me
      while (1);
    }
  }
  Serial.println("MQTT Connected!");
  digitalWrite(LED_BUILTIN, HIGH);
}