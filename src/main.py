from PyQt5.QtWidgets import QApplication
import sys

from ui.UI import Ui

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Ui()
    window.show()
    sys.exit(app.exec_())
