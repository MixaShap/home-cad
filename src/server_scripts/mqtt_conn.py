import time
import paho.mqtt.client as paho

from server_scripts.login_passwd import username, passwd

broker = "tech.esp-one.com"
topics = ["home/device0/tempSensor",
          "home/device0/lightSensor",
          "home/device0/motionSensor",
          "home/device0/waterSensor"]

warning_min_max = [(5, 50), (-1, 1000), (1, 1), (1, 1)]


# define callback
def on_message(client_msg, userdata, message):
    time.sleep(1)
    print("\nReceived msg:")
    print(str(message.topic), str(message.payload.decode("utf-8")))

    for q in range(len(topics)):
        if (message.topic == topics[q]) and (
                (int(message.payload.decode("utf-8")) < int(warning_min_max[q][0])) or
                (int(message.payload.decode("utf-8")) > int(warning_min_max[q][1]))):
            send_alarm(q)


def send_alarm(sensor_number):
    print("Alarm on", topics[sensor_number])
    client.publish("home/alarm", topics[sensor_number])  # publish
    time.sleep(2)


client = paho.Client("HomeCAD")  # create client object

# Bind function to callback
client.on_message = on_message

print("connecting to broker ", broker)
client.username_pw_set(username, passwd)
client.connect(broker)  # connect

client.loop_start()  # start loop to process received messages

print("subscribing...")
for i in range(len(topics)):
    client.subscribe(topics[i])  # subscribe

time.sleep(2)

print("Waiting for data...")

while True:
    time.sleep(0.5)

# print("publishing ")
# client.publish("house/bulb1", "on")  # publish
# time.sleep(4)

# client.disconnect()  # disconnect
# client.loop_stop()  # stop loop
