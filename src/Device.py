import copy

import PyQt5
from PyQt5 import QtWidgets

arduino_templates_path = './res/arduino_templates/'


class Device:

    def __init__(self, device_params, position, ui_link: PyQt5.QtWidgets.QGraphicsPixmapItem = None):
        self.params = copy.deepcopy(device_params)
        self.position = copy.deepcopy(position)
        self.ui_link = ui_link

    def setParams(self, dev_params):
        self.params = copy.deepcopy(dev_params)

    def setUILink(self, ui_link: PyQt5.QtWidgets.QGraphicsPixmapItem):
        self.ui_link = ui_link

    def setPosition(self, coords):
        # print("Old POS", self.position)
        self.position = coords
        print("New POS", self.position)
        self.ui_link.setPos(coords[0], coords[1])

    def getParams(self):
        return self.params

    def getPosition(self):
        return self.position

    def getUI(self):
        return self.ui_link

    def compile(self, filename, wifi=('ssid', 'passwd'),
                mqtt_settings=('tech.esp-one.com', '1883', 'login', 'passwd')):

        with open(arduino_templates_path + 'libs.ino', 'r') as file:
            libs = file.read()

        wifi_str = '#define WLAN_SSID "' + wifi[0] + '"\n' \
                   + '#define WLAN_PASS "' + wifi[1] + '"\n'

        mqtt_str = '#define AIO_SERVER "' + mqtt_settings[0] + '"\n' \
                   + '#define AIO_SERVERPORT ' + mqtt_settings[1] + '\n' \
                   + '#define AIO_USERNAME "' + mqtt_settings[2] + '"\n' \
                   + '#define AIO_KEY "' + mqtt_settings[3] + '"\n'

        with open(arduino_templates_path + 'loop_template.ino', 'r') as file:
            loop_template_str = file.read()

        with open(arduino_templates_path + 'publish_tmp.ino', 'r') as file:
            publish_tmp_str = file.read()

        define_sensors = ''
        feeds_str = ''
        pinModes_str = ''
        subscribers_init_str = ''
        loop_str = ''
        publish_str = ''
        feeds_sub_tmp_str = """Adafruit_MQTT_Subscribe %s = Adafruit_MQTT_Subscribe(&mqtt, "home/device%s/%s");\n"""
        feeds_pub_tmp_str = """Adafruit_MQTT_Publish %s = Adafruit_MQTT_Publish(&mqtt, "home/device%s/%s");\n"""
        pinModes_tmp_str = """  pinMode(%s, OUTPUT);\n  digitalWrite(%s, LOW);\n"""
        pinModes_tmp_read_str = """  pinMode(%s, INPUT_PULLUP);\n"""

        sensors = self.params['sensor']

        if sensors['temperature_checkbox']:
            pinName = 'TEMP'
            sensorName = 'tempSensor'
            define_sensors += '#define TEMP D8\n'
            feeds_str += feeds_pub_tmp_str % (sensorName, filename[-5], pinName)
            pinModes_str += pinModes_tmp_read_str % pinName
            # subscribers_init_str += """  mqtt.subscribe(&%s);\n""" % sensorName
            # loop_str += loop_template_str % (sensorName, sensorName, sensorName, pinName, sensorName, pinName)
            publish_str += publish_tmp_str % (sensorName, pinName, sensorName, sensorName, sensorName)
        if sensors['light_checkbox']:
            pinName = 'LIGHT'
            sensorName = 'lightSensor'
            define_sensors += '#define LIGHT A0\n'
            feeds_str += feeds_pub_tmp_str % (sensorName, filename[-5], pinName)
            pinModes_str += pinModes_tmp_read_str % pinName
            # subscribers_init_str += """  mqtt.subscribe(&%s);\n""" % sensorName
            # loop_str += loop_template_str % (sensorName, sensorName, sensorName, pinName, sensorName, pinName)
            publish_str += publish_tmp_str % (sensorName, pinName, sensorName, sensorName, sensorName)
        if sensors['motion_checkbox']:
            pinName = 'MOTION'
            sensorName = 'motionSensor'
            define_sensors += '#define MOTION D7\n'
            feeds_str += feeds_pub_tmp_str % (sensorName, filename[-5], pinName)
            pinModes_str += pinModes_tmp_read_str % pinName
            # subscribers_init_str += """  mqtt.subscribe(&%s);\n""" % sensorName
            # loop_str += loop_template_str % (sensorName, sensorName, sensorName, pinName, sensorName, pinName)
            publish_str += publish_tmp_str % (sensorName, pinName, sensorName, sensorName, sensorName)
        if sensors['water_checkbox']:
            pinName = 'WATER'
            sensorName = 'waterSensor'
            define_sensors += '#define WATER D6\n'
            feeds_str += feeds_pub_tmp_str % (sensorName, filename[-5], pinName)
            pinModes_str += pinModes_tmp_read_str % pinName
            # subscribers_init_str += """  mqtt.subscribe(&%s);\n""" % sensorName
            # loop_str += loop_template_str % (sensorName, sensorName, sensorName, pinName, sensorName, pinName)
            publish_str += publish_tmp_str % (sensorName, pinName, sensorName, sensorName, sensorName)
        if sensors['light_switch_checkbox']:
            pinName = 'LIGHT_SWITCH'
            sensorName = 'lightSwitch'
            define_sensors += '#define LIGHT_SWITCH D5\n'
            feeds_str += feeds_sub_tmp_str % (sensorName, filename[-5], pinName)
            pinModes_str += pinModes_tmp_str % (pinName, pinName)
            subscribers_init_str += """  mqtt.subscribe(&%s);\n""" % sensorName
            loop_str += loop_template_str % (sensorName, sensorName, sensorName, pinName, sensorName, pinName)
        if sensors['device_checkbox']:
            pinName = 'DEVICE_SWITCH'
            sensorName = 'deviceSwitch'
            define_sensors += '#define DEVICE_SWITCH D4\n'
            feeds_str += feeds_sub_tmp_str % (sensorName, filename[-5], pinName)
            pinModes_str += pinModes_tmp_str % (pinName, pinName)
            subscribers_init_str += """  mqtt.subscribe(&%s);\n""" % sensorName
            loop_str += loop_template_str % (sensorName, sensorName, sensorName, pinName, sensorName, pinName)

        with open(arduino_templates_path + 'mqtt_create.ino', 'r') as file:
            mqtt_create_str = file.read()

        with open(arduino_templates_path + 'setup_start.ino', 'r') as file:
            setup_start_str = file.read()

        with open(arduino_templates_path + 'setup_end.ino', 'r') as file:
            setup_end_str = file.read()

        with open(arduino_templates_path + 'loop_start.ino', 'r') as file:
            loop_start_str = file.read()

        with open(arduino_templates_path + 'connect.ino', 'r') as file:
            connect_str = file.read()

        # collect all strings to one file
        collected_data = libs + '\n' + wifi_str + '\n' + mqtt_str + '\n' \
                         + define_sensors + '\n' + mqtt_create_str + '\n' + feeds_str + '\n' \
                         + setup_start_str + '\n' + pinModes_str + '\n' + setup_end_str + '\n' \
                         + subscribers_init_str + '\n' + loop_start_str + '\n' + loop_str + '  }\n\n' \
                         + publish_str + connect_str

        with open(filename, 'w') as file:
            file.write(collected_data)

        print(filename, 'compiled!')
