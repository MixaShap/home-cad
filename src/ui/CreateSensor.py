import os
from PyQt5.QtWidgets import QDialog
from PyQt5 import uic

GUI_PATH = os.path.abspath('./layout/CreateSensor.ui')


class CreateSensor(QDialog):
    def __init__(self):
        super(CreateSensor, self).__init__()
        uic.loadUi(GUI_PATH, self)

    def return_sensor(self):
        device = {
            'sensor': {
                'temperature_checkbox': self.temperature_box.isChecked(),
                'light_checkbox': self.light_box.isChecked(),
                'motion_checkbox': self.motion_box.isChecked(),
                'water_checkbox': self.water_box.isChecked(),
                'light_switch_checkbox': self.light_switch_box.isChecked(),
                'device_checkbox': self.device_box.isChecked()
            },
            'place': {
                'is_sensor_floor': self.floor_radio.isChecked(),
                'is_sensor_roof': self.roof_radio.isChecked(),
                'is_sensor_wall': self.wall_radio.isChecked(),
                'is_sensor_wall_height': self.wall_txt.text()
            }
        }
        return device

    @staticmethod
    def get_data():
        createSensor = CreateSensor()
        result_state = createSensor.exec_()
        return result_state, createSensor.return_sensor()
