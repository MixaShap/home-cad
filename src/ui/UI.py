import math

from PyQt5 import QtCore, QtWidgets, Qt
from PyQt5 import (QtGui)
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import (QFileDialog, QAction, QMenu)
from PyQt5.QtWidgets import QGraphicsPixmapItem, qApp

import yaml
from typing import List

from ui.CreateSensor import CreateSensor
from Device import Device
from ui.Sensor_UI import RoundItemWithCircle

NO_IMG_PATH = "./res/no_image.png"
click_position = (0, 0)

empty_set = {
    'sensor': {
        'temperature_checkbox': False,
        'light_checkbox': False,
        'motion_checkbox': False,
        'water_checkbox': False,
        'light_switch_checkbox': False,
        'device_checkbox': False
    },
    'place': {
        'is_sensor_floor': False,
        'is_sensor_roof': False,
        'is_sensor_wall': False,
        'is_sensor_wall_height': ''
    }
}


class Ui(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        self.init_scene()
        self.init_menu_bar()
        self.init_context_menu()

        self.devices_list: List[Device] = []

        # debug rectangle
        # rect_item = RoundItemWithCircle(QtCore.QRectF(350, 350, 250, 250))
        # self.scene.addItem(rect_item)

    # noinspection PyAttributeOutsideInit
    def init_scene(self):
        # scene init
        self.scene = QtWidgets.QGraphicsScene(self)
        self.view = QtWidgets.QGraphicsView(self.scene)
        self.img_path = NO_IMG_PATH

        self.view.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        bg_img = QPixmap(self.img_path)
        self.setFixedSize(bg_img.width() + 20, bg_img.height() + 30)
        print(bg_img.width(), bg_img.height())

        self.setCentralWidget(self.view)
        self.graphicsPixmapItem = QGraphicsPixmapItem(bg_img)
        self.scene.addItem(self.graphicsPixmapItem)

        # Init Create Sensor window
        self.create_sensor = CreateSensor()

    def init_menu_bar(self):
        # Create menu bar

        # New event
        new_schema = QAction('&New schema', self)
        new_schema.setShortcut('Ctrl+N')
        new_schema.triggered.connect(self.open_image_file)

        # Open event
        open_schema = QAction('&Open schema', self)
        open_schema.setShortcut('Ctrl+O')
        open_schema.triggered.connect(self.load_from_file)

        # Save event
        save_schema = QAction('&Save schema', self)
        save_schema.setShortcut('Ctrl+S')
        save_schema.triggered.connect(self.save_to_file)

        # exit event
        exitAction = QAction('&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(qApp.quit)

        menu_bar = self.menuBar()
        fileMenu = menu_bar.addMenu('&File')
        fileMenu.addAction(new_schema)
        fileMenu.addAction(open_schema)
        fileMenu.addAction(save_schema)
        fileMenu.addAction(exitAction)

        # Compile event
        compile_schema = QAction('&Compile', self)
        compile_schema.setShortcut('Ctrl+U')
        compile_schema.triggered.connect(self.compile)

        # Optimize event
        optimize_schema = QAction('&Optimize', self)
        optimize_schema.setShortcut('Ctrl+P')
        optimize_schema.triggered.connect(self.optimize)

        compileMenu = menu_bar.addMenu('&Project')
        compileMenu.addAction(optimize_schema)
        compileMenu.addAction(compile_schema)

    # noinspection PyAttributeOutsideInit
    def init_context_menu(self):
        # create context menu
        self.popMenu = QMenu(self)
        self.new_sensor = QAction('New sensor', self)
        self.new_sensor.triggered.connect(self.on_new_sensor_click)
        self.popMenu.addAction(self.new_sensor)

        self.view.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.view.customContextMenuRequested.connect(self.on_context_menu)

    def open_image_file(self):
        print("Open...")
        file_name = QFileDialog.getOpenFileName(self, "Pick schema", filter="Image Files (*.png *.jpg *.bmp *.jpeg)")
        self.set_new_image(file_name[0])
        self.delete_sensors()

    # noinspection PyAttributeOutsideInit
    def set_new_image(self, filename):
        if filename != '':  # if user didn't pick the image don't continue
            print(filename)
            pixmap = QtGui.QPixmap(filename)
            screen_size = QtWidgets.QDesktopWidget().screenGeometry(-1)
            print('pixmap original size:', pixmap.width(), pixmap.height())

            if (pixmap.height() > screen_size.height() * 3 / 4) or (pixmap.width() > screen_size.width() * 3 / 4):
                # width = int(pixmap.width() * (3 * screen_size.width()) / (4 * pixmap.width()))
                # height = int(pixmap.height() * (3 * screen_size.height()) / (4 * pixmap.height()))

                koef_w = (3 * screen_size.width()) / (4 * pixmap.width())
                koef_h = (3 * screen_size.height()) / (4 * pixmap.height())
                if koef_h > koef_w:
                    koef = koef_w
                else:
                    koef = koef_h

                # pixmap = pixmap.scaled(width, height)
                pixmap = pixmap.scaled(int(koef * pixmap.width()), int(koef * pixmap.height()))
                print('screen size:', screen_size.width(), screen_size.height())
                # print('resized:', width, height)

            self.setFixedSize(pixmap.width() + 20, pixmap.height() + 30)
            self.graphicsPixmapItem.setPixmap(pixmap)
            self.img_path = filename

    # show context menu
    def on_context_menu(self, point):
        global click_position
        click_position = point.x(), point.y()
        print("RC", click_position)
        self.popMenu.exec_(self.focusWidget().mapToGlobal(point))

    def on_new_sensor_click(self):
        global click_position
        print("menuItemClicked")
        result_state, device = self.create_sensor.get_data()
        if result_state and (device['sensor'] != empty_set['sensor']) and (device['place'] != empty_set['place']):
            print(device)
            dev = Device(device_params=device, position=click_position)
            self.devices_list.append(dev)
            self.create_new_device(click_position)

    def create_new_device(self, device_position, device_number=-1):
        coef_x = 0
        coef_y = 0

        if self.devices_list[device_number].getParams()['place']['is_sensor_roof']:
            if device_number == -1:
                coef_x = 10 + 125
                coef_y = 5 + 125
            device_item = RoundItemWithCircle(QtCore.QRectF(device_position[0] - coef_x,
                                                            device_position[1] - coef_y, 250, 250))
        elif self.devices_list[device_number].getParams()['place']['is_sensor_wall']:
            radius = int(self.devices_list[device_number].getParams()['place']['is_sensor_wall_height']) / 300 * 250 / 2
            if device_number == -1:
                coef_x = int(10 + radius)
                coef_y = int(5 + radius)
            device_item = RoundItemWithCircle(QtCore.QRectF(device_position[0] - coef_x,
                                                            device_position[1] - coef_y,
                                                            int(radius * 2), int(radius * 2)))
        else:
            if device_number == -1:
                coef_x = 15
                coef_y = 15

            device_item = RoundItemWithCircle(QtCore.QRectF(device_position[0] - coef_x,
                                                            device_position[1] - coef_y, 25, 25))
        self.scene.addItem(device_item)

        # TODO edit menu
        # device_item.mousePressEvent(self.)

        self.devices_list[device_number].setUILink(self.scene.items()[0])
        print('Created in', device_position)

    # def draw_circle(self, sensor):
    #     rect_item = RoundItemWithCircle(QtCore.QRectF(sensor.pos().x() - 35, sensor.pos().y() - 35, 100, 100))
    #     rect_item.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable, True)
    #     self.scene.addItem(rect_item)

    def save_to_dir(self):
        name = QFileDialog.getExistingDirectory(self, 'Compile to folder:')
        return name

    def save_to_file(self):
        name = QFileDialog.getSaveFileName(self, 'Save project', filter='HomeCAD files (*.hcad)')
        print(name[0])
        if name[0] != '':
            saving_data = {'img_path': self.img_path, 'sensors_amount': len(self.devices_list)}
            for i in range(len(self.devices_list)):
                saving_data[i] = {'device' + str(i): {
                    'device_sensors': self.devices_list[i].getParams(),
                    'device_placement': str((self.devices_list[i].getUI().position.x(),
                                             self.devices_list[i].getUI().position.y()))
                }}
            print(saving_data)
            with open(name[0], 'w') as file:
                yaml.dump(saving_data, file)
            print('Saved')

    # noinspection PyAttributeOutsideInit
    def load_from_file(self):
        name = QFileDialog.getOpenFileName(self, 'Open project', filter='HomeCAD files (*.hcad)')
        print(name[0])

        if name[0] != '':
            with open(name[0]) as file:
                loading_data = yaml.safe_load(file)
            self.delete_sensors()

            try:
                self.img_path = loading_data['img_path']
                sensors_amount = loading_data['sensors_amount']
                for i in range(sensors_amount):
                    # print(loading_data[i]['device' + str(i)])
                    device = Device(device_params=loading_data[i]['device' + str(i)]['device_sensors'],
                                    position=eval(loading_data[i]['device' + str(i)]['device_placement']))
                    self.devices_list.append(device)
                    self.create_new_device(device_position=self.devices_list[i].getPosition(), device_number=i)
                self.set_new_image(self.img_path)
            except Exception:
                print('File broken')
            print('Loaded')

    def compile(self):
        compile_path = self.save_to_dir()
        if compile_path != '':

            # TODO create dialog window to ask about wifi passwd and mqtt server settings

            for i in range(len(self.devices_list)):
                filename = compile_path + '/device' + str(i) + '.ino'
                self.devices_list[i].compile(filename)

    def delete_sensors(self):
        for i in range(len(self.devices_list)):
            self.scene.removeItem(self.devices_list[i].getUI())
        self.devices_list.clear()

    def optimize(self):
        sensors_amount = len(self.devices_list)
        center_of_screen = (self.window().size().width() / 2, self.window().size().height() / 2)
        print(center_of_screen)
        print("Amount of sensors:", sensors_amount)
        if sensors_amount == 1:
            print("One dot")

            if self.devices_list[0].getParams()['place']['is_sensor_roof']:
                coef_x = 150
                coef_y = 150
            elif self.devices_list[0].getParams()['place']['is_sensor_wall']:
                radius = int(self.devices_list[0].getParams()['place']['is_sensor_wall_height']) / 300 * 250 / 4
                coef_x = int(150 + radius)
                coef_y = int(150 + radius)
            else:
                coef_x = 150
                coef_y = 150

            sensor_centring = (center_of_screen[0] - coef_x, center_of_screen[1] - coef_y)
            print(sensor_centring)
            self.devices_list[0].setPosition(sensor_centring)

        else:
            middle_radius = 0
            for sensor in self.devices_list:
                print(sensor.position)
                if self.devices_list[0].getParams()['place']['is_sensor_roof']:
                    middle_radius += 125
                elif self.devices_list[0].getParams()['place']['is_sensor_wall']:
                    middle_radius += int(self.devices_list[0].getParams()
                                         ['place']['is_sensor_wall_height']) / 300 * 250 / 4

            if 1 < sensors_amount < 4:
                print("One circle", middle_radius)
                angle = 0
                for q in range(sensors_amount):
                    coef_x = middle_radius * math.sin(math.radians(angle))
                    coef_y = middle_radius * math.cos(math.radians(angle))
                    pos = (center_of_screen[0] - coef_x, center_of_screen[1] - coef_y)
                    self.devices_list[q].setPosition(pos)
                    angle += 120

            elif sensors_amount >= 4:
                print("Circles with dot in center", middle_radius)

                pos = (center_of_screen[0], center_of_screen[1])
                self.devices_list[0].setPosition(pos)

                angle = 0
                for q in range(sensors_amount - 1):
                    coef_x = middle_radius * math.sin(math.radians(angle))
                    coef_y = middle_radius * math.cos(math.radians(angle))
                    pos = (center_of_screen[0] - coef_x, center_of_screen[1] - coef_y)
                    self.devices_list[q].setPosition(pos)
                    angle += 120
