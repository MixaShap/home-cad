from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QGraphicsPixmapItem


class RectItem(QtWidgets.QGraphicsRectItem):
    def paint(self, painter, option, widget=None):
        super(RectItem, self).paint(painter, option, widget)
        painter.save()
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.setBrush(QtCore.Qt.red)
        painter.drawEllipse(option.rect)
        painter.restore()


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        bg_img = QPixmap("../../res/no_image.png")
        super(MainWindow, self).resize(bg_img.width()+20, bg_img.height()+20)
        print(bg_img.width(), bg_img.height())

        scene = QtWidgets.QGraphicsScene(self)
        view = QtWidgets.QGraphicsView(scene)

        self.setCentralWidget(view)
        graphicsPixmapItem = QGraphicsPixmapItem(bg_img)
        scene.addItem(graphicsPixmapItem)

        rect_item = RectItem(QtCore.QRectF(350, 350, 100, 100))
        # rect_item.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable, True)
        scene.addItem(rect_item)


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    # w.resize(640, 480)
    w.show()
    sys.exit(app.exec_())
