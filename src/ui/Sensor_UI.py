import copy

from PyQt5 import QtCore, QtWidgets, Qt
from PyQt5 import QtGui


class RoundItemWithCircle(QtWidgets.QGraphicsEllipseItem):

    position = (0, 0)

    def paint(self, painter, option, widget=None):
        super(RoundItemWithCircle, self).paint(painter, option, widget)
        painter.save()
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.setBrush(QtCore.Qt.green)
        painter.setOpacity(0.5)
        painter.drawEllipse(option.rect)

        chip_img = QtGui.QPixmap("./res/cpu_img.png")
        chip_img = chip_img.scaled(30, 30)
        chip_rect = copy.deepcopy(option.rect)
        chip_rect.setRect(option.rect.x() + option.rect.width()/2 - 15,
                          option.rect.y() + option.rect.width()/2 - 15, 30, 30)
        painter.setOpacity(1.0)
        painter.drawPixmap(chip_rect, chip_img)

        painter.restore()
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable, True)

        print(option.rect.center())
        self.position = option.rect.center()
